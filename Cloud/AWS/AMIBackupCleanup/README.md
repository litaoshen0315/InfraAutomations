This AMI Backup code has refered with a small enhancement:https://gist.github.com/bkozora/724e01903a9ad481d21e

This AMI Cleanup code has refered with a small enhancement:https://gist.github.com/bkozora/d4f1cf0e5cf26acdd377

# Automated AMI Backups

•	This script will first search for all ec2 instances having a tag with "Backup” on it and the instances in "Running" state.

•	As soon as it has the instances list, it loops through each instance and then creates an AMI of it.

•	Also, it will look for a "Retention" tag key which will be used as a retention policy number in days. If there is no tag with that name, it will use a 7 days default value for each AMI.

•	After creating the AMI it creates a "DeleteOn" tag on the AMI indicating when it will be deleted using the Retention value and another Lambda function.


# Automated AMI and Snapshot Deletion

•	This script first searches for all ec2 instances having a tag with "Backup” on it and the instances in "Running" state.

•	As soon as it has the instances list, it loops through each instance and reference the AMIs of that instance.

•	It checks that the latest daily backup succeeded then it stores every image that's reached its DeleteOn tag's date for deletion.

•	It then loops through the AMIs, de-registers them and removes all the snapshots associated with that AMI.


# Important Note:

Please specify your AWS Account Number in the place of "XXXXX" where ever in the code